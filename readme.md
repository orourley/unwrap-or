# unwrap_or

Unwrap-or provides four macros that allow you to use code in the same scope as the calling function. They are similar to Rust's [unwrap_or_else](https://doc.rust-lang.org/std/result/enum.Result.html#method.unwrap_or_else), but the provided code is not in a closure, but in the function itself.

This allows for more flexibility in what you can write. You can have a return, break, or continue statement when an error occurs, without worrying about any complicated return logic to make it happen. You can also directly reference variables in the calling function without having to move them into the closure.

Here's an example of using it:

```rust
fn a_function_which_may_fail() -> Result<String, String> {
	Error("There was an error!")
}

fn another_function() -> Result<(), String> {
	let _ok_value = unwrap_ok_or!(a_function_which_may_fail(), error_value, {
		log(error_value);
		return error_value;
	});
	Ok(())
}
```

The macro above expands to a simple match function:

```rust
let _ok_value = match a_function_which_may_fail() {
	Ok(v) => v,
	Err(error_value) => {
		log(error_value);
		return error_value;
	}
}
```

There are four functions with this syntax:

```rust
let ok_value = unwrap_ok_or!(function_returning_result, error_variable_name, code_to_run_on_error);

let error_value = unwrap_err_or!(function_returning_result, ok_variable_name, code_to_run_on_ok);

let some_value = unwrap_some_or!(function_returning_option, code_to_run_on_none);

let none_value = unwrap_none_or!(function_returning_option, some_variable_name, code_to_run_on_some);
```

